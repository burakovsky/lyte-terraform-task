variable "region" {
  description = "Region where resources will be created."
  type        = string
  default     = "us-east-1"
}

variable "cloudwatch_log_group_name" {
  description = "CloudWatch Log group name to create."
  type        = string
  default     = "/ec2/lyte-app"
}

variable "instance_type" {
  description = "EC2 instance type."
  type        = string
  default     = "t2.micro"
}

variable "repository_name" {
  description = "ECR repository name."
  default     = "lyte-app"
}

variable "image_tag" {
  description = "Docker image tag which will be deployed to EC2 instance."
  type        = string
  default     = "latest"
}

variable "image_digest" {
  description = "Manually setup image digest to install"
  type        = string
  default     = null
}
