## Lyte terraform repository

* Default VPC and subnets are used for EC2 instance.
* ECR, IAM user for Gitlab CI and S3 bucket for terraform state will be created manually. (IAM user credentials are used as environment variables for Gitlab CI runner)

I created 2 EC2 instance for 2 different deploy approach:
1. ec2_auto - new image will deploy automatically after new image will push to Docker registry. ([see deploy stage](https://gitlab.com/burakovsky/lyte-task/-/blob/master/.gitlab-ci.yml)). `HOST` and `SSH_PRIVATE_KEY` variables are used for shh connection to ec2_instance.
2. ec2_manual - for deploying new image run `terraform apply` - terraform get latest image using data_source and update user data for this instance. It will cause instance recreation. Also if you need to deploy particular docker image you can provide its digest using `image_digest` variable.

### Applicstion links
[Automatically updated application](http://3.93.84.237/hello/burakovsky)


[Manually updated application](http://35.171.233.205/hello/burakovsky)