## Region where resources will be created
region = "us-east-1"

# CloudWatch Log Group to create
cloudwatch_log_group_name = "/ec2/lyte-app"

## EC2 instance type
#instance_type = "t2.micro"

## Existing ECR repository where application images are stored
#repository_name = "lyte-app"

## Docker image tag which will be deployed to EC2 instance.
#image_tag = "latest"

## If you eawnt to install particular image which don't have any tag, you can use image digest variable
#image_digest =
