# Use exsisting S3 bucket as terraform backend
terraform {
  backend "s3" {
    bucket  = "lyte-task-terraform-state"
    region  = "us-east-1"
    key     = "terraform.tfstate"
    encrypt = true
    profile = "home"
  }
}

provider "aws" {
  region  = var.region
  profile = "home"
}

# Retrieve default VPC and subnets ids
data "aws_vpc" "default" {
  default = true
}

data "aws_subnet_ids" "default" {
  vpc_id = data.aws_vpc.default.id
}

# Get latest Amazom AMI
data "aws_ami" "amazon" {
  most_recent = true
  owners      = ["137112412989"]
  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-*"]
  }
}

# Retrieve latest app image from ECR
data "aws_ecr_image" "app" {
  count           = var.image_digest == null ? 1 : 0
  repository_name = var.repository_name
  image_tag       = var.image_tag
}

locals {
  image_digest = coalesce(var.image_digest, join("", data.aws_ecr_image.app.*.image_digest))
}

# Create Security Group which allow HTTP access to EC2 instance 
module "sg_http" {
  source = "./modules/security_group"

  name        = "lyte-sg-http"
  description = "Allow HTTP"
  vpc_id      = data.aws_vpc.default.id

  ingress_rules = [
    {
      port_range  = "80"
      protocol    = "TCP"
      cidr_blocks = ["0.0.0.0/0"]
    }
  ]
}

module "sg_ssh" {
  source = "./modules/security_group"

  name        = "lyte-sg-ssh"
  description = "Allow SSH"
  vpc_id      = data.aws_vpc.default.id

  ingress_rules = [
    {
      port_range  = "22"
      protocol    = "TCP"
      cidr_blocks = ["0.0.0.0/0"]
    }
  ]
}

# Create IAM role for EC2 instance
module "lyte_app_iam_role" {
  source = "./modules/iam/role"

  role_name        = "lyte-instance"
  role_description = "Allow access to ECR and CloudWatch logs"
  role_managed_policy_arns = [
    "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryFullAccess",
    "arn:aws:iam::aws:policy/CloudWatchLogsFullAccess"
  ]

  assume_role_policy_data = [
    {
      effect  = "Allow"
      actions = ["sts:AssumeRole"]
      principals = [
        {
          type = "Service"
          identifiers = [
            "ec2.amazonaws.com"
          ]
        }
      ]
    }
  ]
}

# Create CloudWatch log group for docker containers logs
module "cloudwatch_log" {
  source = "./modules/cloudwatch_logs"

  group_name = var.cloudwatch_log_group_name
}

# Create EC2 instance with Nginx proxy for docker and application installed
module "ec2_manual" {
  source = "./modules/ec2"

  name                               = "lyte-app-manual"
  instance_type                      = var.instance_type
  ami                                = data.aws_ami.amazon.id
  subnet_ids                         = data.aws_subnet_ids.default.ids
  associate_static_public_ip_address = true
  iam_instance_profile               = module.lyte_app_iam_role.profile_name
  vpc_security_group_ids             = [module.sg_http.id]
  user_data = templatefile("${path.root}/user_data/user_data_manual.tmpl",
    {
      region         = var.region,
      log_group_name = module.cloudwatch_log.name,
      image_digest   = local.image_digest,
      repo_name      = var.repository_name
    }
  )
  tags = {
    update = "manually"
  }
}

module "ec2_auto" {
  source = "./modules/ec2"

  name                               = "lyte-app-auto"
  instance_type                      = var.instance_type
  ami                                = data.aws_ami.amazon.id
  subnet_ids                         = data.aws_subnet_ids.default.ids
  associate_static_public_ip_address = true
  key_name                           = "burakovsky"
  iam_instance_profile               = module.lyte_app_iam_role.profile_name
  vpc_security_group_ids             = flatten([module.sg_http.id, module.sg_ssh.id])
  user_data = templatefile("${path.root}/user_data/user_data_auto.tmpl",
    {
      region         = var.region,
      log_group_name = module.cloudwatch_log.name,
      repo_name      = var.repository_name
    }
  )
  tags = {
    update = "automatically"
  }
}

output "check" {
  value = <<CHECK
Please check application availability: 
manual update: ${format("http://%s/hello/burakovsky", join("", module.ec2_manual.public_ip))}
auto   update: ${format("http://%s/hello/burakovsky", join("", module.ec2_auto.public_ip))}
CHECK
}
