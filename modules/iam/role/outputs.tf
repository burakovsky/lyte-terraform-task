output "role_name" {
  description = "The name of the role."
  value       = aws_iam_role.role.name
}

output "role_arn" {
  description = "The Amazon Resource Name (ARN) specifying the role."
  value       = aws_iam_role.role.arn
}

output "role_id" {
  description = "The name of the role."
  value       = aws_iam_role.role.id
}

output "role_unique_id" {
  description = "The stable and unique string identifying the role."
  value       = aws_iam_role.role.unique_id
}

output "role_path" {
  description = "The path to the role."
  value       = aws_iam_role.role.path
}

output "role_max_session_duration" {
  description = "The maximum session duration (in seconds) that you want to set for the specified role."
  value       = aws_iam_role.role.max_session_duration
}

output "profile_name" {
  description = "The instance profile's name."
  value       = element(concat(aws_iam_instance_profile.profile.*.name, list("")), 0)
}

output "profile_arn" {
  description = "The ARN assigned by AWS to the instance profile."
  value       = element(concat(aws_iam_instance_profile.profile.*.arn, list("")), 0)
}

output "profile_id" {
  description = "The instance profile's ID."
  value       = element(concat(aws_iam_instance_profile.profile.*.id, list("")), 0)
}

output "profile_path" {
  description = "The path of the instance profile in IAM."
  value       = element(concat(aws_iam_instance_profile.profile.*.path, list("")), 0)
}

output "profile_role" {
  description = "The role assigned to the instance profile."
  value       = element(concat(aws_iam_instance_profile.profile.*.role, list("")), 0)
}

output "profile_unique_id" {
  description = "The unique ID assigned by AWS."
  value       = element(concat(aws_iam_instance_profile.profile.*.unique_id, list("")), 0)
}
