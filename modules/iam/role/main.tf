module "assume_policy" {
  source = "../policy_document"

  policy_data          = var.assume_role_policy_json == null ? var.assume_role_policy_data : []
  policy_id            = var.assume_role_policy_id
  policy_source_json   = var.assume_role_policy_source_json
  policy_override_json = var.assume_role_policy_override_json
  policy_version       = var.assume_role_policy_version
}

module "role_policy" {
  source = "../policy_document"

  policy_data          = var.role_policy_json == null ? var.role_policy_data : []
  policy_id            = var.role_policy_id
  policy_source_json   = var.role_policy_source_json
  policy_override_json = var.role_policy_override_json
  policy_version       = var.role_policy_version
}

resource "aws_iam_role" "role" {
  name                  = var.role_name
  path                  = var.role_path
  description           = var.role_description
  force_detach_policies = var.force_detach_policies
  max_session_duration  = var.max_session_duration
  permissions_boundary  = var.permissions_boundary
  assume_role_policy    = coalesce(var.assume_role_policy_json, element(concat(module.assume_policy.policy_json, list("")), 0))
  tags                  = merge(var.common_tags, var.tags)
}

resource "aws_iam_role_policy" "policy" {
  count  = var.role_policy_json != null || length(var.role_policy_data) > 0 ? 1 : 0
  name   = coalesce(var.policy_name, "${aws_iam_role.role.name}-policy")
  role   = aws_iam_role.role.id
  policy = coalesce(var.role_policy_json, element(concat(module.role_policy.policy_json, list("")), 0))
}

resource "aws_iam_role_policy_attachment" "managed" {
  count      = length(var.role_managed_policy_arns)
  role       = aws_iam_role.role.name
  policy_arn = var.role_managed_policy_arns[count.index]
}

resource "aws_iam_instance_profile" "profile" {
  count = var.create_iam_instance_profile ? 1 : 0
  name  = var.role_name
  path  = var.role_path
  role  = aws_iam_role.role.name
}
