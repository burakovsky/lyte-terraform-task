variable "role_name" {
  description = "The name of the role. If omitted, Terraform will assign a random, unique name."
  default     = null
}

variable "create_iam_instance_profile" {
  description = "Create or not IAM Instance profile"
  type        = bool
  default     = true
}

variable "role_description" {
  description = "The description of the role."
  default     = null
}

variable "role_path" {
  description = "The path to the role. See IAM Identifiers for more information."
  default     = null
}

variable "force_detach_policies" {
  description = "Specifies to force detaching any policies the role has before destroying it."
  type        = bool
  default     = true
}

variable "max_session_duration" {
  description = "The maximum session duration (in seconds) that you want to set for the specified role."
  type        = number
  default     = null
}

variable "permissions_boundary" {
  description = "The ARN of the policy that is used to set the permissions boundary for the role."
  default     = null
}

variable "tags" {
  description = "Key-value mapping of tags for the IAM role."
  type        = map(string)
  default     = {}
}

variable "assume_role_policy_id" {
  description = "An ID for the policy document."
  default     = null
}

variable "assume_role_policy_source_json" {
  description = "An IAM policy document to import as a base for the current policy document."
  default     = null
}

variable "assume_role_policy_override_json" {
  description = "An IAM policy document to import and override the current policy document."
  default     = null
}

variable "assume_role_policy_version" {
  description = "IAM policy document version. Valid values: 2008-10-17, 2012-10-17."
  default     = "2012-10-17"
}

variable "assume_role_policy_json" {
  description = "The policy that grants an entity permission to assume the role (in JSON format)."
  default     = null
}

variable "assume_role_policy_data" {
  description = "The policy that grants an entity permission to assume the role. Used for `aws_iam_policy_document` data source."
  default     = []
}

variable "role_managed_policy_arns" {
  description = "The ARNs of the policy you want to apply."
  type        = list(string)
  default     = []
}

variable "role_policy_json" {
  description = "The policy document. This is a JSON formatted string."
  default     = null
}

variable "role_policy_data" {
  description = "The policy document. Used for `aws_iam_policy_document` data source."
  default     = []
}

variable "policy_name" {
  description = "The name of the role policy."
  default     = null
}

variable "role_policy_id" {
  description = "An ID for the policy document."
  default     = null
}

variable "role_policy_source_json" {
  description = "An IAM policy document to import as a base for the current policy document."
  default     = null
}

variable "role_policy_override_json" {
  description = "An IAM policy document to import and override the current policy document."
  default     = null
}

variable "role_policy_version" {
  description = "IAM policy document version. Valid values: 2008-10-17, 2012-10-17."
  default     = "2012-10-17"
}

variable "common_tags" {
  description = "Common tags which are added to all resources"
  type        = map(any)
  default     = {}
}
