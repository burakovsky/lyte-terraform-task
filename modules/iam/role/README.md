## AWS IAM role Terraform module

> Note: Only terraform v0.12.0 and newer is supported

### Usage
```hcl
module "example_role" {
  source = "git::https://git.office.comscore.com/scm/cloud-infra/cloud-terraform.git//modules/aws/iam_role"

  role_name        = "example-role"

  assume_role_policy_data = [
    {
      effect  = "Allow"
      actions = ["sts:AssumeRole"]
      principals = [
        {
          type        = "AWS",
          identifiers = ["arn:aws:iam::123456789:root"]
        }
      ]
    }
  ]

  role_policy_name = "AllowedAccess"
  role_policy_data = [
    {
      effect    = "Allow"
      actions   = ["ec2:*"]
      resources = ["*"]
    }
  ]
}
```

### Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| assume\_role\_policy\_data | The policy that grants an entity permission to assume the role. Used for `aws_iam_policy_document` data source. | list(any) | `"[]"` | no |
| assume\_role\_policy\_id | An ID for the policy document. | string | `"null"` | no |
| assume\_role\_policy\_json | The policy that grants an entity permission to assume the role (in JSON format). | string | `"null"` | no |
| assume\_role\_policy\_override\_json | An IAM policy document to import and override the current policy document. | string | `"null"` | no |
| assume\_role\_policy\_source\_json | An IAM policy document to import as a base for the current policy document. | string | `"null"` | no |
| assume\_role\_policy\_version | IAM policy document version. Valid values: 2008-10-17, 2012-10-17. | string | `"2012-10-17"` | no |
| create\_iam\_instance\_profile | Create or not IAM Instance profile. | bool | `"true"` | no |
| common_tags | Common tags which are added to all resources | map(any) | `"{}"` | no |
| managed\_policy\_arns | The ARNs of the policy you want to apply. | list(string) | `"[]"` | no |
| role\_description | The description of the role. | string | `"null"` | no |
| role\_force\_detach\_policies | Specifies to force detaching any policies the role has before destroying it. | bool | `"false"` | no |
| role\_max\_session\_duration | The maximum session duration (in seconds) that you want to set for the specified role. | number | `"null"` | no |
| role\_name | The name of the role. If omitted, Terraform will assign a random, unique name. | string | `"null"` | no |
| role\_policy\_data | The policy document. Used for aws_iam_policy_document data source. | list(any) | `"[]"` | no |
| role\_policy\_json | The policy document. This is a JSON formatted string. | string | `"null"` | no |
| role\_policy\_name | The name of the role policy. | string | `"null"` | no |
| role\_policy\_override\_json | An IAM policy document to import and override the current policy document. ([Details](https://www.terraform.io/docs/providers/aws/d/iam_policy_document.html#example-with-source-and-override)) | string | `"null"` | no |
| role\_path | The path to the role. See [IAM Identifiers](https://docs.aws.amazon.com/IAM/latest/UserGuide/reference_identifiers.html) for more information. | string | `"/"` | no |
| role\_permissions\_boundary | The ARN of the policy that is used to set the permissions boundary for the role. | string | `"null"` | no |
| role\_policy\_id | An ID for the policy document. | string | `"null"` | no |
| role\_policy\_source\_json | An IAM policy document to import as a base for the current policy document. ([Details](https://www.terraform.io/docs/providers/aws/d/iam_policy_document.html#example-with-source-and-override)) | string | `"null"` | no |
| role\_policy\_version | IAM policy document version. Valid values: 2008-10-17, 2012-10-17. | string | `"2012-10-17"` | no |
| role\_tags | Key-value mapping of tags for the IAM role. | map(any) | `"null"` | no |

### Outputs

| Name | Description |
|------|-------------|
| profile\_arn | The ARN assigned by AWS to the instance profile. |
| profile\_id | The instance profile's ID. |
| profile\_name | The instance profile's name. |
| profile\_path | The path of the instance profile in IAM. |
| profile\_role | The role assigned to the instance profile. |
| profile\_unique\_id | The unique ID assigned by AWS. |
| role\_arn | The Amazon Resource Name (ARN) specifying the role. |
| role\_id | The name of the role. |
| role\_name | The name of the role. |
| role\_max\_session\_duration | The maximum session duration (in seconds) that you want to set for the specified role. |
| role\_path | The path of the role. |
| role\_unique\_id | The stable and unique string identifying the role. |