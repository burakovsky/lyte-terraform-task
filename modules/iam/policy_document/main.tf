data "aws_iam_policy_document" "this" {
  count = var.policy_data != [] ? 1 : 0

  policy_id     = var.policy_id
  source_json   = var.policy_source_json
  override_json = var.policy_override_json
  version       = var.policy_version

  dynamic "statement" {
    for_each = var.policy_data

    content {
      sid           = lookup(statement.value, "sid", null)
      effect        = title(lookup(statement.value, "effect", "Allow"))
      actions       = lookup(statement.value, "actions", null)
      not_actions   = lookup(statement.value, "not_actions", null)
      resources     = lookup(statement.value, "resources", null)
      not_resources = lookup(statement.value, "not_resources", null)

      dynamic "principals" {
        for_each = lookup(statement.value, "principals", [])

        content {
          type        = lookup(principals.value, "type")
          identifiers = lookup(principals.value, "identifiers")
        }
      }

      dynamic "not_principals" {
        for_each = lookup(statement.value, "not_principals", [])

        content {
          type        = lookup(not_principals.value, "type")
          identifiers = lookup(not_principals.value, "identifiers")
        }
      }

      dynamic "condition" {
        for_each = lookup(statement.value, "condition", [])

        content {
          test     = lookup(condition.value, "test")
          variable = lookup(condition.value, "variable")
          values   = lookup(condition.value, "values")
        }
      }
    }
  }
}
