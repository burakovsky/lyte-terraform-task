variable "policy_data" {
  description = "The policy document. Used for `aws_iam_policy_document` data source."
  default     = []
}

variable "policy_id" {
  description = "An ID for the policy document."
  default     = null
}

variable "policy_source_json" {
  description = "An IAM policy document to import as a base for the current policy document."
  default     = null
}

variable "policy_override_json" {
  description = "An IAM policy document to import and override the current policy document."
  default     = null
}

variable "policy_version" {
  description = "IAM policy document version. Valid values: 2008-10-17, 2012-10-17."
  default     = "2012-10-17"
}
