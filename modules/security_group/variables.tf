variable "name" {
  type        = string
  description = "The name of the security group. If omitted, Terraform will assign a random, unique name"
}

variable "description" {
  description = "The security group description. Defaults to 'Managed by Terraform'. Cannot be \"\" ."
  type        = string
  default     = null
}

variable "vpc_id" {
  description = "The VPC ID."
  type        = string
}

variable "tags" {
  description = "A mapping of tags to assign to the resource."
  type        = map(any)
  default     = {}
}

variable "revoke_rules_on_delete" {
  description = "Instruct Terraform to revoke all of the Security Groups attached ingress and egress rules before deleting the rule itself."
  type        = bool
  default     = false
}

variable "ingress_rules" {
  description = "Security group ingress rules."
  type        = any
  default     = []
}

variable "egress_rules" {
  description = "Security group egress rules."
  type        = any
  default = [
    {
      port_range  = 0
      protocol    = "-1"
      cidr_blocks = ["0.0.0.0/0"]
    }
  ]
}
