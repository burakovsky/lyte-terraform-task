output "id" {
  description = "The ID of the security group."
  value       = aws_security_group.group.id
}

output "arn" {
  description = "The ARN of the security group."
  value       = aws_security_group.group.arn
}

output "vpc_id" {
  description = "The VPC ID."
  value       = aws_security_group.group.vpc_id
}

output "name" {
  description = "The name of the security group."
  value       = aws_security_group.group.name
}

output "ingress" {
  description = "The ingress rules. See above for more."
  value       = aws_security_group.group.ingress
}

output "egress" {
  description = "The egress rules. See above for more."
  value       = aws_security_group.group.egress
}
