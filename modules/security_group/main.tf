resource "aws_security_group" "group" {
  name                   = var.name
  description            = var.description
  vpc_id                 = var.vpc_id
  revoke_rules_on_delete = var.revoke_rules_on_delete
  tags                   = merge(map("Name", var.name), var.tags)

  dynamic "ingress" {
    for_each = var.ingress_rules

    content {
      from_port        = lookup(ingress.value, "protocol") != "icmp" ? tonumber(element(split("-", tostring(ingress.value.port_range)), 0)) : lookup(ingress.value, " icmp_type_number", "-1")
      to_port          = lookup(ingress.value, "protocol") != "icmp" ? tonumber(element(reverse(split("-", tostring(ingress.value.port_range))), 0)) : lookup(ingress.value, " icmp_code", "-1")
      protocol         = lookup(ingress.value, "protocol", "tcp")
      cidr_blocks      = lookup(ingress.value, "cidr_blocks", null)
      ipv6_cidr_blocks = lookup(ingress.value, "ipv6_cidr_blocks", null)
      prefix_list_ids  = lookup(ingress.value, "prefix_list_ids", null)
      security_groups  = lookup(ingress.value, "security_groups", null)
      self             = lookup(ingress.value, "self", false)
      description      = lookup(ingress.value, "description", null)
    }
  }

  dynamic "egress" {
    for_each = var.egress_rules

    content {
      from_port        = lookup(egress.value, "protocol") != "icmp" ? tonumber(element(split("-", tostring(egress.value.port_range)), 0)) : lookup(egress.value, " icmp_type_number", "-1")
      to_port          = lookup(egress.value, "protocol") != "icmp" ? tonumber(element(reverse(split("-", tostring(egress.value.port_range))), 0)) : lookup(egress.value, " icmp_code", "-1")
      protocol         = lookup(egress.value, "protocol", "tcp")
      cidr_blocks      = lookup(egress.value, "cidr_blocks", null)
      ipv6_cidr_blocks = lookup(egress.value, "ipv6_cidr_blocks", null)
      prefix_list_ids  = lookup(egress.value, "prefix_list_ids", null)
      security_groups  = lookup(egress.value, "security_groups", null)
      self             = lookup(egress.value, "self", false)
      description      = lookup(egress.value, "description", null)
    }
  }
}
