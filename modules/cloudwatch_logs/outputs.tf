output "arn" {
  description = "The Amazon Resource Name (ARN) specifying the log group."
  value       = aws_cloudwatch_log_group.group.arn
}

output "name" {
  description = "Name of the log group."
  value       = aws_cloudwatch_log_group.group.name
}
