resource "aws_cloudwatch_log_group" "group" {
  name              = var.group_name
  name_prefix       = var.group_name == null ? var.group_name_prefix : null
  retention_in_days = var.log_retention_in_days
  kms_key_id        = var.kms_key_id
  tags              = var.tags
}
