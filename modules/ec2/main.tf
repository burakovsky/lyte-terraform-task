# Check if ec2 instance has a T2/T3 instance type and skip credit_specification variable if it doesn't.
locals {
  is_t2_instance       = replace(var.instance_type, "t2", "") != var.instance_type
  is_t3_instance       = replace(var.instance_type, "t3", "") != var.instance_type
  credit_specification = local.is_t2_instance || local.is_t3_instance ? var.credit_specification : {}
}

resource "aws_instance" "this" {
  count = var.instance_count

  ami                                  = var.ami
  instance_type                        = var.instance_type
  user_data                            = var.user_data
  subnet_id                            = length(var.boot_network_interfaces) == 0 ? element(var.subnet_ids, count.index) : null
  key_name                             = var.key_name
  monitoring                           = var.monitoring
  vpc_security_group_ids               = length(var.boot_network_interfaces) == 0 ? var.vpc_security_group_ids : null
  iam_instance_profile                 = var.iam_instance_profile
  associate_public_ip_address          = var.associate_static_public_ip_address ? true : var.associate_dynamic_public_ip_address
  private_ip                           = var.private_ip
  ebs_optimized                        = var.ebs_optimized
  source_dest_check                    = var.source_dest_check
  disable_api_termination              = var.disable_api_termination
  instance_initiated_shutdown_behavior = var.instance_initiated_shutdown_behavior
  placement_group                      = var.placement_group
  get_password_data                    = var.get_password_data
  tenancy                              = var.tenancy
  host_id                              = var.dedicated_host_id
  cpu_core_count                       = var.cpu_core_count
  cpu_threads_per_core                 = var.cpu_threads_per_core
  ipv6_address_count                   = var.ipv6_addresses != null ? length(var.ipv6_addresses) : var.ipv6_address_count
  ipv6_addresses                       = var.ipv6_addresses
  tags                                 = merge(map("Name", (var.instance_count > 1) ? format("%s-%d", var.name, count.index + 1) : var.name), var.tags)
  volume_tags                          = merge(var.tags, var.volume_tags)

  dynamic "root_block_device" {
    for_each = var.root_block_device

    content {
      volume_type           = lookup(root_block_device.value, "volume_type", null)
      volume_size           = lookup(root_block_device.value, "volume_size", null)
      iops                  = lookup(root_block_device.value, "volume_type", null) == "io1" ? lookup(root_block_device.value, "iops", null) : null
      delete_on_termination = lookup(root_block_device.value, "delete_on_termination", true)
      encrypted             = lookup(root_block_device.value, "encrypted", false)
      kms_key_id            = lookup(root_block_device.value, "encrypted", false) == true ? lookup(root_block_device.value, "kms_key_id", null) : null
    }
  }

  dynamic "ebs_block_device" {
    for_each = var.ebs_block_devices

    content {
      device_name           = lookup(ebs_block_device.value, "device_name")
      volume_type           = lookup(ebs_block_device.value, "volume_type", null)
      volume_size           = lookup(ebs_block_device.value, "volume_size", null)
      snapshot_id           = lookup(ebs_block_device.value, "snapshot_id", null)
      iops                  = lookup(ebs_block_device.value, "volume_type", null) == "io1" ? lookup(ebs_block_device.value, "iops", null) : null
      delete_on_termination = lookup(ebs_block_device.value, "delete_on_termination", true)
      encrypted             = lookup(ebs_block_device.value, "encrypted", false)
      kms_key_id            = lookup(ebs_block_device.value, "encrypted", false) == true ? lookup(ebs_block_device.value, "kms_key_id", null) : null
    }
  }

  dynamic "ephemeral_block_device" {
    for_each = var.ephemeral_block_devices

    content {
      device_name  = lookup(ephemeral_block_device.value, "device_name")
      virtual_name = lookup(ephemeral_block_device.value, "virtual_name", null)
      no_device    = lookup(ephemeral_block_device.value, "no_device", null)
    }
  }

  dynamic "network_interface" {
    for_each = var.boot_network_interfaces

    content {
      device_index          = lookup(network_interface.value, "device_index")
      network_interface_id  = lookup(network_interface.value, "network_interface_id")
      delete_on_termination = false
    }
  }

  dynamic "credit_specification" {
    for_each = local.credit_specification

    content {
      cpu_credits = credit_specification.value
    }
  }
}

resource "aws_eip" "static" {
  count = var.associate_static_public_ip_address ? var.instance_count : 0

  instance                  = element(aws_instance.this.*.id, count.index)
  associate_with_private_ip = var.private_ip
  vpc                       = true
  tags                      = merge(map("Name", (var.instance_count > 1) ? format("%s-%d-ip", var.name, count.index + 1) : "${var.name}-ip"), var.tags)
}

