output "id" {
  description = "List of IDs of instances"
  value       = aws_instance.this.*.id
}

output "arn" {
  description = "List of ARNs of instances"
  value       = aws_instance.this.*.arn
}

output "availability_zone" {
  description = "List of availability zones of instances"
  value       = aws_instance.this.*.availability_zone
}

output "key_name" {
  description = "List of key names of instances"
  value       = aws_instance.this.*.key_name
}

output "placement_group" {
  description = "List of placement groups of instances"
  value       = aws_instance.this.*.placement_group
}

output "public_dns" {
  description = "List of public DNS names assigned to the instances. For EC2-VPC, this is only available if you've enabled DNS hostnames for your VPC"
  value       = aws_instance.this.*.public_dns
}

output "public_ip" {
  description = "List of public IP addresses assigned to the instances, if applicable"
  value       = coalescelist(aws_eip.static.*.public_ip, aws_instance.this.*.public_ip)
}

output "primary_network_interface_id" {
  description = "List of IDs of the primary network interface of instances"
  value       = aws_instance.this.*.primary_network_interface_id
}

output "private_dns" {
  description = "List of private DNS names assigned to the instances. Can only be used inside the Amazon EC2, and only available if you've enabled DNS hostnames for your VPC"
  value       = aws_instance.this.*.private_dns
}

output "private_ip" {
  description = "List of private IP addresses assigned to the instances"
  value       = aws_instance.this.*.private_ip
}

output "ipv6_addresses" {
  description = "List of IPv6 addresses assigned to the instances"
  value       = aws_instance.this.*.ipv6_addresses
}

output "security_groups" {
  description = "List of associated security groups of instances"
  value       = aws_instance.this.*.security_groups
}

output "vpc_security_group_ids" {
  description = "List of associated security groups of instances, if running in non-default VPC"
  value       = aws_instance.this.*.vpc_security_group_ids
}

output "subnet_id" {
  description = "List of IDs of VPC subnets of instances"
  value       = aws_instance.this.*.subnet_id
}

output "credit_specification" {
  description = "List of credit specification of instances"
  value       = aws_instance.this.*.credit_specification
}

output "tags" {
  description = "List of tags of instances"
  value       = aws_instance.this.*.tags
}

output "volume_tags" {
  description = "List of volume tags of instances"
  value       = aws_instance.this.*.volume_tags
}

output "password_data" {
  description = "List of password data of instances"
  value       = aws_instance.this.*.password_data
}
